import numpy as np
import queue as q

"""
Vitse Maxime (apprenti)

questions 15/19 => codage huffman (RAF fix decode_carac) & LZ (RAF 15 à 19)

3 fichiers sont présents dans le dossier :
- run_compression_sans_pertes.txt
- encode_carac_sur_recherche_lorem.txt
- encode_carac_sur_recherche_temps_perdu.txt
"""


## codage Huffman
def compteur_caracteres(txt):
    # question 3
    str_txt = ""
    with open(txt, 'r', encoding='utf8') as text:
        for ligne in text:
            str_txt += ligne

    # question 1 : list_probas de abracadabra -> {'a': 5, 'b': 2, 'r': 2, 'c': 1, 'd': 1}
    occurrences = {}
    for i in str_txt:
        if i in occurrences:
            occurrences[i] += 1
        else:
            occurrences[i] = 1
    return occurrences


def entropie(list_probas):  # question 2 : eg. - 5/11 * log2(5/11) - 2/11 * log2(2/11) ....
    e = 0
    total = 0
    for i in list_probas.values():
        total += i
    for i in list_probas.values():
        e += (i / total) * np.log2(1 / (i / total))
    return e


class HuffNoeud:  # question 4
    def __init__(self, w):
        self.poids = w
        self.caractere = None
        self.lcode = ''
        self.gauche = None
        self.droit = None

    def __lt__(self, other):
        return self.poids < other.poids

    def __repr__(self, code_parent=''):
        if self.caractere is not None:
            return self.caractere + ": " + code_parent + self.lcode + '\n'
        else:
            return self.gauche.__repr__(code_parent + self.lcode) \
                   + self.droit.__repr__(code_parent + self.lcode)


def arbre_huffman(d):  # question 4
    queue = q.PriorityQueue()

    for key in d:
        noeud = HuffNoeud(d[key])
        noeud.caractere = key
        queue.put((d[key], noeud))

    while queue.qsize() >= 2:
        gauche = queue.get()[1]
        gauche.lcode = '0'
        droite = queue.get()[1]
        droite.lcode = '1'
        noeud = HuffNoeud(gauche.poids + droite.poids)
        noeud.gauche = gauche
        noeud.droit = droite
        queue.put((noeud.poids, noeud))

        # print(noeud)
    return queue.get()[1]


def codage_arbre(arbre, codeparent=""):  # question 5/6
    if arbre.caractere is not None:
        return {arbre.caractere: codeparent + arbre.lcode}
    else:
        dict = {}
        dict.update(codage_arbre(arbre.gauche, codeparent + str(arbre.gauche.lcode)))
        dict.update(codage_arbre(arbre.droit, codeparent + str(arbre.droit.lcode)))
        return dict


def dict_codage(dict):  # question "6"
    return codage_arbre(arbre_huffman(dict))


def encode_carac(path_text_file, path_write_file, dict):  # question 8
    str_txt = ""
    with open(path_text_file, 'r', encoding='utf8') as text:
        for ligne in text:
            str_txt += ligne
    coded_text = ""
    for i in str_txt:
        for key in dict:
            if i == key:
                coded_text = coded_text + dict[key]
    file = open(path_write_file, "w+")
    file.write(coded_text)
    file.close()
    return "encode_carac for the file " + path_text_file + " is done in file " + path_write_file


def decode_carac(path_encode_text_file, path_write_file, arbre_huff):  # question 11
    str_txt = ""
    with open(path_encode_text_file, 'r', encoding='utf8') as text:
        for ligne in text:
            str_txt += ligne
    decoded_text = ""
    for i in str_txt:
        for key in arbre_huff:
            if i == key:
                decoded_text = decoded_text + arbre_huff.caractere
    file = open(path_write_file, "w+")
    file.write(decoded_text)
    file.close()
    return "decode_carac for the file " + path_encode_text_file + " is done in file " + path_write_file


def octet2s(octet):  # question 10
    i = int.from_bytes(octet, byteorder='big')
    q = 1
    string_bin = ''
    while q != 0:
        q = i // 2
        r = i % 2
        string_bin = repr(r) + string_bin
        i = q
    return string_bin


## codage LZ
# question 14 : cherche la plus longue sous-chaıne de maximum m caracteres contigus de r, etant prefixe
def recherche_prefixe(r, s, m):
    d = 0
    if len(r) <= m:
        return len(r), m
    else:
        for m in range(len(r)):
            for n in range(len(s)):
                if r[m] == s[n]:
                    d += 1
        l = len(r) - m

    return d, l


if __name__ == '__main__':
    # print(compteur_caracteres("abracadabra"))
    # print(entropie({'a': 5, 'b': 2, 'r': 2, 'c': 1, 'd': 1}))
    print("compteur_caracteres + entropie de recherche_temps_perdu : ")
    path_rtp = "recherche_temps_perdu.txt"
    print(compteur_caracteres(path_rtp))
    print(entropie(compteur_caracteres(path_rtp)))
    print("Entropie lorem: ")
    print(entropie(compteur_caracteres("lorem.txt")))

    # codage 4* le nombre de lettres => % de gain sur le disque ? 7.5 Mo => 4Mo (fichier final environ 55% du fichier original)
    # print(os.path.getsize(path_rtp))

    # question 7
    print("codage_arbre recherche_temps_perdu: ")
    print(codage_arbre(arbre_huffman(compteur_caracteres(path_rtp))))
    # print(dict_codage(compteur_caracteres(path_rtp)))

    # question 9 // 12
    print(encode_carac("lorem.txt",
                       "test_encode_carac_sur_recherche_lorem.txt",
                       dict_codage(compteur_caracteres("lorem.txt"))))

    print("codage_arbre lorem: ")
    print(codage_arbre(arbre_huffman(compteur_caracteres("lorem.txt"))))

    # question 13
    print(decode_carac("test_encode_carac_sur_recherche_lorem.txt",
                       "test_decode_carac_sur_recherche_lorem.txt",
                       dict_codage(compteur_caracteres("lorem.txt"))
                       ))

    # question 10 test
    print("octet2s : ")
    print(octet2s(b'\xF1\x10'))

    # question 16
    print("recherche_prefixe sur les chaınes r = baa et s = aaaaaaaaaaa :")
    print(recherche_prefixe("baa", "aaaaaaaaaaa", 3))

