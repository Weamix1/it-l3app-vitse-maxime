from PIL import Image
from scipy.fftpack import dct, idct
import numpy as np
from collections import Counter

"""
Vitse Maxime (apprenti)

questions 11 / 14 (RAF mat2prejpeg et prejpeg2mat)
"""


def display_picture_and_convert_ycbcr():
    img = Image.open("chaton.png").convert('YCbCr')  # questions 1 & 2
    img.show()

    y, cb, cr = img.split()  # question 3
    widthCb, heightCb = cb.size
    widthCr, heightCr = cr.size

    cb.resize((int(widthCb / 2), int(heightCb / 2)))  # question 4
    cr.resize((int(widthCr / 2), int(heightCr / 2)))

    y.show()
    cb.show()
    cr.show()


def mat_dct(m):  # question 5
    # matrice -> frequences = > matrice spectrale
    # pixel en haut à gauche : changement de basses frequences (valeur moyenne de votre image en terme de pixel)
    # celle juste à droite de celle : à quel point la moitié gauche change
    return dct(dct(m, axis=0, norm="ortho"), axis=1, norm="ortho")


def mat_idct(m):  # question 6
    return idct(idct(m, axis=0, norm="ortho"), axis=1, norm="ortho")


def quantification(m):  # question 7
    return (np.multiply(Q, np.rint(np.divide(m, Q)))).astype(int)


def zigzag(m):  # question 8
    res = dict(sum(map(Counter, m), Counter()))
    res = sorted(res.items(), key=lambda t: t[::-1])
    matrice = []
    for a_tuple in res:
        matrice.append(a_tuple[0])
    matrice = np.array(matrice)
    return matrice


def dezigzag(m):  # question 9
    matrice = m.split(" ")
    for i in range(0, len(matrice)):
        matrice[i] = int(matrice[i])
    res = np.asarray(matrice)
    return res


if __name__ == '__main__':
    Q = np.load("Q.npy")
    print("Matrice Q :")
    print(Q)
    display_picture_and_convert_ycbcr()

    # print(type(Q))  # <class 'numpy.ndarray'>
    # array = np.reshape(Q, (50, 104))
    # data = Image.fromarray(array)
    # data.show()

    # pour créer un np.array de taille n par m rempli de zeros : np.zeros((n,m))
    img = Image.open("chaton.png")
    m = img.load()

    # print(img.size)
    # pixel_values = list(img.getdata())
    # print(pixel_values)
    # print(m)
    # print(m[0, 0])

    print("Zigzag sur Q:")
    print(zigzag(Q))

    # question 10
    print("Mat_dct sur Q:")
    print(mat_dct(Q))
    print("Quantification sur Q:")
    print(quantification(Q))
    #print("Mat_idct sur Q:")
    #print(mat_idct(quantification(mat_dct)))