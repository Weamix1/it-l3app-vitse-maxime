import numpy as np
import matplotlib.pyplot as plt

def s1(t): #signal d'une note pure
    a = 2 #amplitude
    f = 3 #fréquence en Hz
    res = a*np.sin(2*np.pi*f*t)
    return res

def plot_signal(s, N, ta, tb):
    fig = plt.figure()
    Ts = np.linspace(ta,tb,N)
    plt.plot(Ts, [s(t) for t in Ts])
    plt.grid()
    
def main():
    N= 1000
    tA = 0
    tB = 10

