import numpy as np
import matplotlib.pyplot as plt

def s1(t): #signal d'une note pure
    a = 2 #amplitude
    f = 3 #frequence en Hz
    res = a*np.sin(2*np.pi*f*t)
    return res

def plot_signal(s, N=1000, ta=0, tb=10):
    plt.figure()
    Ts = np.linspace(ta,tb,N)
    plt.plot(Ts, [s(t) for t in Ts]) # x et y
    plt.grid()
    plt.show()

def plot_courbe_enroulee(v,s, N=1000, ta=0, tb=10):
    plt.figure()
    Ts = np.linspace(ta,tb,N)
    lx=[s(t)*np.cos(2*np.pi*v*t) for t in Ts]
    ly=[s(t)*np.sin(2*np.pi*v*t) for t in Ts]
    mx=np.mean(lx)
    my=np.mean(ly)
    plt.plot(lx,ly)
    plt.grid()
    plt.show()

#correction 
""" def plot_courbe_enroulee(v, s, N=1000, a=0, b=10):
    plt.figure()
    Ts = np.linspace(a,b,N)
    st = np.array([s(t) for t in Ts])
    pts = np.array([[st[i]*np.cos(2*np.pi*v*Ts[i]),
                     st[i]*np.sin(2*np.pi*v*Ts[i])] for i in range(N)])
    plt.plot(pts[:,0], pts[:,1])

    amplitude = np.max(abs(st))
    plt.xlim(-amplitude,amplitude)
    plt.ylim(-amplitude,amplitude)
    plt.grid()
    m = np.mean(pts, 0)
    plt.plot(m[0], m[1], 'ro')  """   
    
def main():
    #plot_signal(s1)
    plot_courbe_enroulee(1,s1)

if __name__ == "__main__":
    main()