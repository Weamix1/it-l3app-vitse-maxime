# Vitse Maxime (apprenti)

## TP_COMPRESSION :
run in length coding = compter le nombre de caractères qui se répétent (fonctionne bien s’il y a beaucoup de répétitions).

questions 15/19 => codage huffman (RAF fix decode_carac) & LZ (RAF 15 à 19)

3 fichiers sont présents dans le dossier :
- run_compression_sans_pertes.txt
- encode_carac_sur_recherche_lorem.txt
- encode_carac_sur_recherche_temps_perdu.txt

## TP_IMAGES 
questions 11 / 14 (RAF mat2prejpeg et prejpeg2mat)


1 fichier est présent dans le dossier :
run_compression_images.txt

## TP_FOURIER : 
technique pour décomposer un signal sonore pour retrouver les notes et leurs intensités 